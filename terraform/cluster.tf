provider google {
	credentials = "${file(var.google_account)}"
	project = "${var.project}"
	region = "${var.region}"
	zone = "${var.zone}"
}

data google_compute_network default_network {
	name = "default"
}

data google_compute_image os_image {
	family = "${var.os_family}"
	project = "${var.os_project}"
}

resource google_compute_firewall external_ssh {
	name = "${var.prefix}-external-ssh"
	network = data.google_compute_network.default_network.self_link
	allow {
		protocol = "tcp"
		ports = ["22"]
	}
	source_ranges = ["0.0.0.0/0"]
	target_tags   = ["ssh-accessible"]
}

resource google_compute_instance databases {
	count = var.replica_count
	name = "${var.prefix}-db-${count.index}"
	machine_type = var.machine_type
	tags = ["ssh-accessible"]
	metadata = {
        ssh-keys = "${var.ssh_user}:${file(var.ssh_public_key)}"
    }
	boot_disk {
		initialize_params {
			image = data.google_compute_image.os_image.self_link
		}
	}
	network_interface {
		network = data.google_compute_network.default_network.self_link
		access_config {
		}
	}
	depends_on = [google_compute_firewall.external_ssh]
}

resource google_compute_instance application {
	name = "${var.prefix}-app"
	machine_type = var.machine_type
	tags = ["ssh-accessible", "http-server"]
	metadata = {
        ssh-keys = "${var.ssh_user}:${file(var.ssh_public_key)}"
    }
	boot_disk {
		initialize_params {
			image = data.google_compute_image.os_image.self_link
		}
	}
	network_interface {
		network = data.google_compute_network.default_network.self_link
		access_config {
		}
	}
	depends_on = [google_compute_firewall.external_ssh]
}

locals {
	instance_ids = concat(
		google_compute_instance.databases.*.id,
		[google_compute_instance.application.id]
	)
	instance_names = concat(
		google_compute_instance.databases.*.name, 
		[google_compute_instance.application.name]
	)
	public_ips = concat(
		google_compute_instance.databases.*.network_interface.0.access_config.0.nat_ip, 
		[google_compute_instance.application.network_interface.0.access_config.0.nat_ip]
	)
	app_public_ip = google_compute_instance.application.network_interface.0.access_config.0.nat_ip
	app_private_ip = google_compute_instance.application.network_interface.0.network_ip
	db_private_ips = google_compute_instance.databases.*.network_interface.0.network_ip
	repmgr_master_ip = google_compute_instance.databases.0.network_interface.0.network_ip
}

resource null_resource cluster {
	count = length(local.instance_ids)
	triggers = {
    	cluster_instance_ids = "${join(",", local.instance_ids)}"
	}
	connection {
		type = "ssh"
		user = "${var.ssh_user}"
		private_key = "${file(var.ssh_private_key)}"
		host = "${local.public_ips[count.index]}"
	}
	provisioner "file" {
		source = "../scripts"
		destination = "/tmp"
	}
	provisioner "remote-exec" {
		inline = [
			"chmod +x /tmp/scripts/*.sh",
			"export GITLAB_HA_SOLUTION=\"${var.gitlab_ha_solution}\"",
			"export GITLAB_APP_CONFIG=\"${var.gitlab_app_config}\"",
			"export GITLAB_DB_CONFIG=\"${var.gitlab_db_config}\"",
			"export GITLAB_TOKEN=\"${var.gitlab_token}\"",
			"export GITLAB_ARTIFACT=\"${var.gitlab_artifact}\"",
			"export GITLAB_DOMAIN=\"${coalesce(var.gitlab_domain, "http://${local.app_public_ip}")}\"",
			"export NODE_IPS=\"${join(" ", local.db_private_ips)}\"",
			"export NODE_CIDRS=\"${join(" ", formatlist("%s/32", local.db_private_ips))}\"",
			"export REPMGR_MASTER_IP=\"${local.repmgr_master_ip}\"",
			"export PGBOUNCER_IP=\"${local.app_private_ip}\"",
			"export PGBOUNCER_CIDR=\"${local.app_private_ip}/32\"",
			"export NUMBER_OF_REPLICAS=${var.replica_count}",
			"export NUMBER_OF_INSTANCES=${var.replica_count + 1}",
			"/tmp/scripts/install.sh ${local.instance_names[count.index]}",
			"/tmp/scripts/configure.sh ${local.instance_names[count.index]}",
			"/tmp/scripts/post-install.sh ${local.instance_names[count.index]}",
			"/tmp/scripts/patch.sh ${local.instance_names[count.index]}"
		]
	}
}

resource google_compute_instance_group instance_group {
	name = "${var.prefix}-instances"
	network = data.google_compute_network.default_network.self_link
	instances = google_compute_instance.databases.*.self_link
}
