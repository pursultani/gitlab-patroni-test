variable project {
}

variable region {
}

variable zone {
}

variable prefix {
	default = "patroni-test"
}

variable google_account {
}

variable replica_count {
	type = number
	default = 3
}

variable machine_type {
    default = "n1-standard-1"
}

variable os_family {
	default = "debian-10"
}

variable os_project {
	default = "debian-cloud"
}

variable ssh_user {
}

variable ssh_private_key {
}

variable ssh_public_key {
}

variable gitlab_token {
}

variable gitlab_artifact {
}

variable gitlab_domain {
	default = null
}

variable gitlab_app_config {
	default = "gitlab.app.rb"
}

variable gitlab_db_config {
	default = "gitlab.patroni.rb"
}

variable gitlab_ha_solution {
	default = "patroni"
}
