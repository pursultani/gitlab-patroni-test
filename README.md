# GitLab HA with Patroni

This repository contains provisioning and configuration scripts for setting up a GitLab HA cluster with Patroni and repmgr.

**NOTE:**: Some of the codes that are used here are still under active development. You should not use this repository
          for production purposes.

## Structure

* `terraform/`: This directory contains Terraform 0.12 configuration to set up a GCP cluster.
* `scripts/`: Installation and configuration scripts that are used by Terraform Provisioners to configure the GitLab HA
   cluster.

## Usage

### Terraform Variables

You can control the configuration with the following variables. Some of these variables are required and you must
specify a value.

| Variable | Description | Default |
|-|-|-|
| `project` | The GCP project all of your resources will be created in. | **Required** |
| `zone` | The the default location for GCP regional resources. | **Required** |
| `region` | The the default location for GCP zonal resources. | **Required** |
| `google_account` | The the file path of GCP account credentials. | **Required** |
| `prefix`  | A prefix to name all the resources that are created for this cluster. | `patroni-test` |
| `machine_type` | The default machine type for all VM instances. | `n1-standard-1` |
| `os_project` | The OS project to look up the default OS image. | `debian-cloud` |
| `os_family` | The OS family to look up the default OS image. | `debian-cloud` |
| `replica_count` | Number of database replicas. Each replica is deployed on a separate VM. Eventually one of them becomes the leader. | `3` |
| `gitlab_artifact` | The URL of GitLab Debian package. | **Required** |
| `gitlab_token` | Your GitLab token for downloading the package when it is not publicly available. | _Optional_ |
| `gitlab_domain` | The domain name of the cluster. It is used to set `external_url`. When it is not provided the IP address of the host will be used. | _Optional_ |
| `gitlab_app_config` | The relative path of `gitlab.rb` template for the application node | `gitlab.app.rb` |
| `gitlab_db_config` | The relative path of `gitlab.rb` template for the database nodes | `gitlab.db.rb` |
| `gitlab_ha_solution` | The database HA solution to use. Either `patroni` or `repmgr`. | `patroni` |
| `ssh_user` | Your SSH user to login to GCE instances | **Required** |
| `ssh_private_key` | The local path to your private SSH key. It must be unencrypted. | **Required** |
| `ssh_public_key` | The local path to your public SSH key. | **Required** |

### Installation and Configuration

The Terraform script will create `replica_count` database nodes, named **db-_n_**, and one application node, named
**app**. PostgreSQL and Patroni/repmgr are configured to work exclusively on database nodes. The Rails application and
pgbouncer are deployed and configured to work on application node.

After building all instances, the Terraform Provisioner executes the following scripts on all of them in order:

1. `scripts/install.sh`: Downloads and installs GitLab package.
2. `scripts/configure.sh`: Writes `gitlab.rb` using the templates and calls `gitlab-ctl reconfigure`.
3. `scripts/post-install.sh`: Runs post-installation steps, e.g. `gitlab-rake gitlab:db:configure`.

**NOTE:** Before you start, make sure that your GCP credentials are correctly stored in `terraform/account.json`.

**NOTE:** The configuration script uses default passwords. You can change them in `scripts/password.sh`.

**NOTE:** You can change the configuration of database and application nodes in `scripts/configure.sh`. See
`gitlab_app_config` and `gitlab_db_config` variables.

**NOTE:** You can choose to use repmgr or Patroni for database HA. This is particularly useful for testing switching
between solutions.

```bash
terraform init # Only for the first time
terraform apply -var-file=${MY_VARIABLES_FILE}
```
