#!/bin/bash

set -xe

sudo apt install -y git
sudo cp /tmp/scripts/*.patch /opt/gitlab/embedded/
cd /opt/gitlab/embedded/
sudo git apply *.patch || true
