roles ['postgres_role']

patroni['enable'] = true

postgresql['listen_address'] = '0.0.0.0'

postgresql['pgbouncer_user_password'] = '${PGBOUNCER_PASSWORD_HASH}'
postgresql['sql_user_password'] = '${POSTGRESQL_PASSWORD_HASH}'
postgresql['trust_auth_cidr_addresses'] = %w(127.0.0.1/32 ${NODE_CIDRS})
postgresql['md5_auth_cidr_addresses'] = %w(127.0.0.1/32 ${PGBOUNCER_CIDR})

consul['services'] = %w(postgresql)
consul['configuration'] = {
  server: true,
  bootstrap_expect: ${NUMBER_OF_REPLICAS},
  retry_join: %w(${NODE_IPS})
}

gitlab_rails['auto_migrate'] = false

gitlab_exporter['enable'] = false
sidekiq['enable'] = false
unicorn['enable'] = false
puma['enable'] = false
registry['enable'] = false
gitaly['enable'] = false
gitlab_workhorse['enable'] = false
nginx['enable'] = false
prometheus_monitoring['enable'] = false
redis['enable'] = false
