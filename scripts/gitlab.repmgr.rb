roles ['consul_role', 'postgres_role']

gitlab_exporter['enable'] = false

gitlab_rails['auto_migrate'] = false

postgresql['version'] = 11
postgresql['listen_address'] = '0.0.0.0'
postgresql['hot_standby'] = 'on'
postgresql['wal_level'] = 'replica'
postgresql['shared_preload_libraries'] = 'repmgr_funcs'
postgresql['max_wal_senders'] = ${NUMBER_OF_INSTANCES}
postgresql['max_replication_slots'] = ${NUMBER_OF_INSTANCES}

postgresql['pgbouncer_user_password'] = '${PGBOUNCER_PASSWORD_HASH}'
postgresql['sql_user_password'] = '${POSTGRESQL_PASSWORD_HASH}'
postgresql['trust_auth_cidr_addresses'] = %w(127.0.0.1/32 ${NODE_CIDRS})
postgresql['md5_auth_cidr_addresses'] = %w(127.0.0.1/32 ${PGBOUNCER_CIDR})

repmgr['node_name'] = '${NODE_NAME}'
repmgr['trust_auth_cidr_addresses'] = %w(127.0.0.1/32 ${NODE_CIDRS})

consul['services'] = %w(postgresql)
consul['configuration'] = {
  server: true,
  bootstrap_expect: ${NUMBER_OF_REPLICAS},
  retry_join: %w(${NODE_IPS})
}
