#!/bin/bash

set -xe

source "${0%/*}/passwords.sh"

render_config() {
  export NODE_NAME="${1}"
  perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' < "${2}"
}

extra_config() {
  if [[ "${GITLAB_HA_SOLUTION}" = 'repmgr' && "${1}" != *-db-0 ]]; then
    echo "repmgr['master_on_initialization'] = false"
  fi
}

gitlab_config() {
  if [[ "${1}" == *-app ]]; then
    render_config "${1}" "${0%/*}/${GITLAB_APP_CONFIG}"
  else
    render_config "${1}" "${0%/*}/${GITLAB_DB_CONFIG}"
  fi
  extra_config "${1}"
}

gitlab_config "${1}" | sudo tee /etc/gitlab/gitlab.rb

sudo gitlab-ctl reconfigure
