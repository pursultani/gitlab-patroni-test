external_url '${GITLAB_DOMAIN}'

postgresql['enable'] = false
grafana['enable'] = false
gitlab_exporter['enable'] = false
pgbouncer['enable'] = true
consul['enable'] = true

gitlab_rails['db_host'] = '127.0.0.1'
gitlab_rails['db_port'] = 6432
gitlab_rails['db_password'] = '${POSTGRESQL_PASSWORD}'
gitlab_rails['auto_migrate'] = false

pgbouncer['admin_users'] = %w(pgbouncer gitlab-consul)
pgbouncer['users'] = {
  'gitlab-consul': {
    password: '${CONSUL_PASSWORD_HASH}'
  },
  'pgbouncer': {
    password: '${PGBOUNCER_PASSWORD_HASH}'
  }
}

consul['watchers'] = %w(postgresql)
consul['configuration'] = {
  retry_join: %w(${NODE_IPS})
}
