# Change the default password

export CONSUL_USERNAME='gitlab-consul'
export CONSUL_PASSWORD='s3cret'
export CONSUL_PASSWORD_HASH="$(echo "${CONSUL_PASSWORD}" | gitlab-ctl pg-password-md5 ${CONSUL_USERNAME})"

export PGBOUNCER_USERNAME='pgbouncer'
export PGBOUNCER_PASSWORD='s3cret'
export PGBOUNCER_PASSWORD_HASH="$(echo "${PGBOUNCER_PASSWORD}" | gitlab-ctl pg-password-md5 ${PGBOUNCER_USERNAME})"

export POSTGRESQL_USERNAME='gitlab'
export POSTGRESQL_PASSWORD='s3cret'
export POSTGRESQL_PASSWORD_HASH="$(echo "${POSTGRESQL_PASSWORD}" | gitlab-ctl pg-password-md5 ${POSTGRESQL_USERNAME})"
