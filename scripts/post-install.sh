#!/bin/bash

set -xe

source "${0%/*}/passwords.sh"

if [[ "${1}" == *-app ]]; then
  sleep 30
  printf '127.0.0.1:*:pgbouncer:%s:%s' "${PGBOUNCER_USERNAME}" "${PGBOUNCER_PASSWORD}" \
    | sudo -u "${CONSUL_USERNAME}" tee /var/opt/gitlab/consul/.pgpass
  sudo chmod 600 /var/opt/gitlab/consul/.pgpass
  sudo gitlab-ctl restart consul
  sudo gitlab-rake gitlab:db:configure
elif [[ "${GITLAB_HA_SOLUTION}" = 'repmgr' && "${1}" != *-db-0 ]]; then
  sleep 10
  sudo gitlab-ctl repmgr standby setup "${MASTER_IP}" -w
fi
